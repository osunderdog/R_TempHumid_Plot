########################################
#
#  This is available under creative commons.
#  Author: Bruce W. Lowther
#  brucelowther@gmail.com
#
##################################################
require(ggplot2)
require(scales)
require(plyr)

source('dataload_functions.R')

dataPath <- "./data"
xrefPath <- "."
graphPath <- "./graphics"
graphPostFix <- "_plot"

plotFilename <- function(path = graphPath, extension=".png")
  {
    return(file.path(path,
                     paste(getDateString(),
                           graphPostFix,
                           extension, sep="", delim="")
                     )
           )
  }

xrefFilename <- "location_xref.csv"

##Started out with an IQT approach for eliminating outliers
##now that I have more data, ouliers are just points outside
##the normal operating range of temperature or humidity.
sane.range.dt <- data.frame(type=c('HUMID','TEMP'),
                            low=c(0,-20),
                            high=c(100,120))


##keep the last xx days for plotting
plotRange <- as.difftime(20, units="days")

dataFiles.d <- list.files(dataPath, full.names=TRUE, all.files=FALSE)
dataFiles <- dataFiles.d[!file.info(dataFiles.d)$isdir]
dataFiles
##exclude anything with _Err
dataFiles.good <- dataFiles[!grepl("_Err$", dataFiles) & !grepl("xref", dataFiles)]

##Load them up using ldply 
ag.df.raw <- NULL
ag.df.raw <- ldply(dataFiles.good,single.table.read)
str(ag.df.raw)

range(ag.df.raw$dt)
##need to make 'dev' a hex number identifying the xbee. (dl)

loc.xref.df <- NULL
loc.xref.df <- xref.table.read(file.path(xrefPath,xrefFilename));

##merge by id -- which will double up some records,  then we'll narrow
##using startdt and enddt 
ag.df.m <- merge(ag.df.raw,loc.xref.df, by=c('id','type'))
ag.df.m$keep.merge <- with(ag.df.m, dt >startdt & dt < enddt)
table(ag.df.m$keep.merge)
ag.df <- subset(ag.df.m,ag.df.m$keep.merge)

#Keep the last xx points based on the plotRange.
ag.df$keep.recent <- with(ag.df, (dt > range(dt)[2] - plotRange))
ag.df.limit <- subset(ag.df, ag.df$keep.recent)

#apply sanity filter for temperature and humidity readings.
ag.df.sl <- merge(ag.df.limit, sane.range.dt,
                  by=c('type'))
str(ag.df.sl)
#keep only the ones that are betwen the sane limits for the type.
ag.df.sl$keep.sl <- with(ag.df.sl, (val >= low & val <= high))
table(ag.df.sl$keep.sl)
ag.df.sane <- subset(ag.df.sl,ag.df.sl$keep.sl)

##Double scale -- both temp and humidity... would be better as two
##top/bottom charts.
## Ploting the last 10 days... 
g <- ggplot(ag.df.sane,
            aes(x=dt, y=val,group=location, colour=location))
g <- g + scale_x_datetime(major = "2 days",
                          minor= "12 hours", format = "%Y-%m-%d %H")
g <- g + opts(axis.text.x = theme_text(angle = 90))
g <- g + geom_line()
g <- g + facet_wrap(~type,scales = "free_y", nrow=2)
#g <- g +  stat_smooth()

##Printing to a file that I can look at periodically.
##going to run this automatically every day or so.
png(file=plotFilename(), width = 1200, height = 700, units = "px")
print(g)
dev.off()

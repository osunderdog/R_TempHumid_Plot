
##
## Single Table Data Read old
##
## Expected columns:
##  dt         datetime (2013-12-10 16:45:00)
##  dev        identifier for the device (xbee source address)
##  val        the value measured
##  volt       some reference voltage if needed. (just checking)
##
## Don't assume data has a header because the data is many appended
## data sets without a header.
##
single.table.read.old <- function(fn) 
{
  ##changed number of columns recent.
  
  result <- read.table(fn, header=FALSE, 
                       sep="\t", 
                       colClasses = c('POSIXct', 'numeric', 'numeric', 'numeric'),
                       col.names=c('dt', 'dev', 'val', 'volt'))
  result$fname <- as.factor(fn)
  result$id <- as.factor(sprintf("%X",result$dev))
  result$type <- as.factor('TEMP')
  result$dev <- NULL
  return(result)
}


##
## Single Table Data Read new
##
## Expected columns:
##  dt         datetime (2013-12-10 16:45:00)
##  dev        identifier for the device (xbee source address)
##  type       some type string (TEMP, HUMID, etc)
##  val        the value measured
##  volt       some reference voltage if needed. (just checking)
##
## Don't assume data has a header because the data is many appended
## data sets without a header.
##
single.table.read.new <- function(fn) 
{
  result <- read.table(fn, header=FALSE, 
                       sep="\t", 
                       colClasses = c('POSIXct', 'numeric', 'factor', 'numeric','numeric'),
                       col.names=c('dt', 'dev', 'type', 'val', 'volt'))
  result$fname <- as.factor(fn)
  result$id <- as.factor(sprintf("%X",result$dev))
  result$dev <- NULL
  return(result)
}

##handle failures -- not sure how to cascade these when the time comes
##-- the error function would have to have a nested tryCatch
single.table.read <- function(fn)
{
  print(fn)
  ##try the old read first.
  result <- tryCatch(single.table.read.old(fn), 
                     error=function(e) single.table.read.new(fn))
  return(result)
}



xref.table.read <- function(fn)
{
  ##read in the xref table for cases where a sensor changes from
  ##only location to another.

  ##Expected Columns
  ##  sensorid     identifier for the device (xbee source address)
  ##  startdt      start time the sensor was at this location
  ##  enddt        end time that the sensor was at this location
  ##  location     description of the location
  ##  type         sensor measurement type (TEMP, HUMID, etc)
  result <- read.table(fn,
             header=TRUE,
             sep=",",
             colClasses=c('numeric','POSIXct', 'POSIXct', 'factor', 'factor'))
  ## Again, converting the sensorid to an that is hex
  result$id <- as.factor(sprintf("%X",result$sensorid))
  result$sensorid <- NULL
  
  return(result)
}


##standard sortable time format for filenames
##Optional parameter is the system time to generate a timestamp for.
##If not provided, then the current time is used to create the
##timestamp 
getDateString <- function(x = Sys.time())
  {
    return(format.POSIXct(x, format="%Y%m%d%H%M%S"))
  }

